<html>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font-family: Arial,Helvetica,sans-serif;
            text-align: left;
            width: 960px;
            margin: 0 auto;
        }
        #title {
            color: #000000;
            margin-top: 50px;
            margin-bottom: 10px;
            text-shadow: 2px 2px 5px #CCCCCC;
        }    
        pre {
            text-align: left;  
            white-space: pre-wrap;       /* css-3 */
            white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
            white-space: -pre-wrap;      /* Opera 4-6 */
            white-space: -o-pre-wrap;    /* Opera 7 */
            word-wrap: break-word;       /* Internet Explorer 5.5+ */
            margin-top: 10px;
                   }
        #content {width: 1000px;}
        .source {
            border: 1px solid gray;
        }
    </style>

    <body>
        <div id="container">
            <div id="title">
                <h1>Xtends - Multiple Inheritance</h1>
            </div>
            <div id="content">
                    Multiple inheritanhe it's not possible with PHP, only Symfony have an approach called Mixins.
                    <br /><br />
                    Many frameworks have a Base_controller and all controllers inherit from it. When you have a controller that have common methods, but you want to change few lines of this class, you must to edit it or duplicate it in another class.  
                    <br /> <br />
                    With Xtends you can have all common functionality inherit from Base_controller and own functionality, added, overrided or partialy overrided.
                    <br /> <br />
                    <br />
                    <h2>How it work's</h2>
                    <br />
                    Normally, your controller (we are using Myclass for this example), extend from a Base_controller:
                    <pre>class Myclass extends Base_controller</pre>
                    <br />
                    Xtends add a intermediate class to allow multiple inheritance:
                    <pre>class Myclass extends Base_loader</pre>
                    <br />
                    In your app/Myclass.php, make a call to xtends_loader method: 
                    <pre>
public function __construct()
{
    parent::__construct();
   <b>$this->xtends_loader(__CLASS__,__FILE__);</b>
}
                    </pre>
                    <br />
                    This try to load same Myclass.php from another directory (BASEPATH), parse it and load as "MyClass_xtends"
                    <br /><br />
                    Method that exists in app/Myclass, takes precedence.<br />
                    Method that not exists in app/Myclass, but exists in base/Myclass, base/Myclas/Method are excuted.<br />
                    Method that not exists in app/Myclass, neither base/Myclass, but exists in Base_controller, is executed.<br />
                    
                    <br />
                    <br />
                    In first demo, a normal inheritance, Myclass extends Base_controller...<a href="demoMyClassBase.php">base/Myclass without inheritance</a>
                    <br />
                    <i>base/Myclass extends from Base_Controller</i>
                    <br />
                    <br />
                    Second demo is a copy of base/Myclass, located in app/MyClass. <a href="demoMyClassApp.php">Myclass Xtends inheritance</a>
                    <br />
                    <i>app/MyClass extends from Base_Loader, and also from base/MyClass and also from Base_Controller</i>
                    <br />
                    <br />
                    Third demo is an empty extend from base/Myclass2, located in app/MyClass2. <a href="demoMyClassApp2.php">Myclass2 Xtends inheritance (2)</a>
                    <br />
                    <i>app/Myclass2 doesn't have any method, inherit all methods from base/MyClass2</i>
                    <br />
                    <br />
                    You can take a look inside each demo files:
                    <br />
                    <br />
                    Source of 1st demo
                    <div class="source">
                        <?php highlight_file('demoMyClassBase.php'); ?>
                    </div>
                    <br />
                    <br />
                    Source of 2nd demo
                    <div class="source">
                        <?php highlight_file('demoMyClassApp.php'); ?>
                    </div>
                    <br />
                    <br />
                    Source of app/Myclass.php
                    <div class="source">
                        <?php highlight_file('app/Myclass.php'); ?>
                    </div>
                    <br />
                    <br />
                    Source of base/Myclass.php
                    <div class="source">
                        <?php highlight_file('base/Myclass.php'); ?>
                    </div>
                    <br />
                    <br />
                    Source of app/Myclass2.php
                    <div class="source">
                        <?php highlight_file('app/Myclass2.php'); ?>
                    </div>
                    <br />
                    <br />
                    Source of base/Myclass2.php
                    <div class="source">
                        <?php highlight_file('base/Myclass2.php'); ?>
                    </div>
            </div>
    </body>
</html>
<?php

require_once __DIR__.'/Base_controller.php';
/**
 * This is an example of a base controller
 * 
 */
class Base_Loader extends Base_Controller
{
   	/* Common module extender object */
	public $class_xtends;
	

	/**
	 * Get properties from the common module, otherwise, from $APP
	 * 
	 * @param type $myVar
	 * @return var
	 * @throws Exception
	 */
	public function __get($myVar)
	{
		if (isset($this->class_xtends->$myVar))
			return $this->class_xtends->$myVar;
		if (isset(parent::$APP->$myVar))
			return parent::$APP->$myVar;
		throw new Exception('No such var: ' . $myVar);		
	}
	
	/**
	 * Set properties to a var inside the common module, only if exists
	 * 
	 * @param type $myVar
	 * @param type $myValue
	 */
	public function __set($myVar,$myValue='')
	{
		if (isset($this->class_xtends->$myVar))
			$this->class_xtends->$myVar = $myValue;
		else
			parent::$APP->$myVar = $myValue;
	}

	/**
	 * Call any method inside common module, else call $APP method
	 * 
	 * @param type $name
	 * @param array $arguments
	 * @return type
	 * @throws Exception
	 */
	public function __call($name, array $arguments) {
        if (method_exists($this->class_xtends, $name)) {
            return call_user_func_array(array($this->class_xtends, $name), $arguments);
        }
        if (method_exists(parent::$APP, $name)) {
            return call_user_func_array(array(parent::$APP, $name), $arguments);
        }
       throw new Exception('No such method: ' . $name);		
	}
	
	/**
	 * Common module extender
	 * 
	 * @param type $class
	 * @param type $module
	 * @param type $params
	 */
	public function xtends_loader($class, $module='', $params='')
	{
		$currentPath = $module;
		$currentPath = str_replace('\\','/',$currentPath);
		$appPath = str_replace('\\','/',realpath(APPPATH));
		$commonPath = str_replace('\\','/',realpath(BASEPATH));
		$currentPath = str_replace( $appPath,$commonPath,$currentPath);
		if (file_exists($currentPath))
		{
			$moduleEclass_xtends = file_get_contents($currentPath);
			$moduleEclass_xtends = str_ireplace('class '.$class,'class '.ucfirst($class)."_xtends",$moduleEclass_xtends);
			$moduleEclass_xtends=preg_replace("/<\?php|<\?|\?>/", "", $moduleEclass_xtends);
			eval($moduleEclass_xtends);
			
			$newclass = ucfirst($class)."_xtends"; 
			$this->class_xtends = new $newclass($params);
		}

	}
}
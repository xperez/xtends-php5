<?php

require_once __DIR__.'/../base/Base_controller.php';

class Myclass2 extends Base_Controller
{
    public $where = "We are in base/MyClass2";
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function test1($param1,$param2)
    {
        return $this->where.' => '.'Test1 from base/Myclass2, params: '.$param1.'-'.$param2;
    }
}

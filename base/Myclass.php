<?php

require_once __DIR__.'/../base/Base_controller.php';

class Myclass extends Base_Controller
{
    public $mynum = 567;

    public function __construct()
    {
        parent::__construct();
    }
    
    public function Mymethod1()
    {
        return "MyMethod1 from base/".get_class($this)." using base/Myclass";
    }

    public function Mymethod2()
    {
        return "MyMethod2 from base/".get_class($this)." using base/Myclass";
    }
    
    public function Mymethod4()
    {
        return "MyMethod4 from base/".get_class($this)." using base/MyClass";
    }
    
}
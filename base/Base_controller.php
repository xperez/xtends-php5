<?php

class Base_Controller
{
    public static $APP;
    
    public $mynum = 123;
    
    public function __construct()
    {
        self:$APP = $this;
    }
    
    public function Mymethod1()
    {
        return "MyMethod1 from base/".get_class($this)." using Base_controller";
    }
    public function Mymethod2()
    {
        return "MyMethod2 from base/".get_class($this)." using Base_controller";
    }
    public function Mymethod3()
    {
        return "MyMethod3 from base/".get_class($this)." using Base_controller";
    }
    
}


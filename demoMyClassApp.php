<?php

define('APPPATH',__DIR__.'/app/');
define('BASEPATH',__DIR__.'/base/');

require __DIR__.'/app/Myclass.php';


$myApp = new Myclass();

// Mymethod1 exists in Base_controller, base/myclass and app/myclass, but is returned the last.
echo $myApp->Mymethod1()."<br>";

// Mymethod2 exists in Base_controller and base/myclass and app/myclass, , but is returned the last.
//  In app/myclass 
echo $myApp->Mymethod2()."<br>";

// Mymethod3 exists only in Base_loader
echo $myApp->Mymethod3()."<br>";

// Mymethod4 exists only in Base_controller
echo $myApp->Mymethod4()."<br>";

// Now, another example of multiple inheritance declarations, try to comment the lines:
// 1) public $mynum on app/myclass, reload this page, yuo will see '123'
// 2) public $mynum on base/Base_controller, reload this page, yuo will see '567' (declared in base/myclass)
echo 'Value of $mynum var: '.$myApp->mynum;


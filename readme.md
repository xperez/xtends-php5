# XTends Multiple inheritance for PHP5

Multiple inheritanhe it's not possible with PHP, only Symfony have an approach called Mixins.

Many frameworks have a Base_controller and all controllers inherit from it. When you have a controller that have common methods, but you want to change few lines of this class, you must to edit it or duplicate it in another class.  

With Xtends you can have all common functionality inherit from Base_controller and own functionality, added, overrided or partialy overrided.

## How it work's

Normally, your controller (we are using Myclass for this example), extend from a Base_controller:
```python
     class Myclass extends Base_controller
```

Xtends add a intermediate class to allow multiple inheritance:
```python
     class Myclass extends Base_loader
```

When you load your Myclass.php, calls 
```python
    $this->xtends_loader(__CLASS__,__FILE__);
```

This try to load same Myclass.php from another directory (BASEPATH), parse it and load as "MyClass_common"

Method that exists in app/Myclass, takes precedence.

Method that not exists in app/Myclass, but exists in base/Myclass, base/Myclas/Method are excuted.
Method that not exists in app/Myclass, neither base/Myclass, but exists in Base_controller, is executed.

In first demo (demoMyClassBase.php), a normal inheritance, Myclass extends Base_controller.

Second demo (demoMyClassApp.php) is a copy of base/Myclass, show extended inheritance, located in app/MyClass.

Trird demo (app/MyClass2), is an empty extend (app/MyClass2) from base/Myclass2.

Online demo: [Demo](http://www.4amics.com/libraries/xtends/)


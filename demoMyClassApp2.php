<?php

define('APPPATH',__DIR__.'/app/');
define('BASEPATH',__DIR__.'/base/');

require __DIR__.'/app/Myclass2.php';


$myApp = new Myclass2();

// Call a non existing method in app/Myclass, trought xtends->base/Myclass2.php
echo $myApp->test1('Xtends',' PHP5 multi-inheritance').'<br />';
echo 'Where ? '.$myApp->where.'<br />';
echo 'Accessing directly: '.$myApp->class_xtends->where.'<br />';
echo "UNSET var \$where, myApp will inherit from base/\$where <br />";
unset ($myApp->where);
echo 'Where ? '.$myApp->where.'<br />';

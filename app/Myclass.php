<?php

require_once __DIR__.'/../base/Base_loader.php';

class Myclass extends Base_Loader
{
    public $mynum = 890;

    public function __construct()
    {
        parent::__construct();
        $this->xtends_loader(__CLASS__,__FILE__);
    }
    
    public function Mymethod1()
    {
        return "MyMethod1 from app/".get_class($this);
    }
    
    public function Mymethod2()
    {
        return $this->class_xtends->Mymethod2()." <b>example post-adding funtionality</b>";
    }

    public function Mymethod3()
    {
        return "<b>Example pre-adding functionality:</b> ".parent::Mymethod3();
    }
}